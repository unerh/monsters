'use strict';

const readline = require('readline');
const request = require('request-promise');
const DungeonServer = require('./api/dungeon-server');
const Player = require('./api/models/player');

const rl = readline.createInterface({
  input: process.stdin,
  output: process.stdout
});

const checkPlayerHealth = (player) => {
  return new Promise((resolve, reject) => {
    if (player.isAlive()) {
      getMovement().then(movePlayer).then(checkPlayerHealth);
    } else {
      console.log('Game Over!');
      player.stats();
      reject();
      process.exit();
    }
  });
}

const movePlayer = (location) => {
  return new Promise((resolve) => {
    request(`http://localhost:8080/room/${location.x}/${location.y}`).
    then((res) => {
      if (res === 'GOLD') { 
        player.foundGold();
      } else if (res === 'MONSTER') {
        player.foundMonster();
      }
      console.log(`${res}!!`);
      player.stats();
      resolve(player);
    });
  });
}

const getMovement = () => {
  return new Promise((resolve, reject) => {
    rl.question('Please choose direction: ', direction => { 
      if (!direction.match(/north|south|east|west/)) {
        console.log('You must enter one of north/south/east/west!');
        reject();
        checkPlayerHealth(player);
      } else {
        player.move(direction);
        resolve(player.location);
      }
    });
  });
}

new DungeonServer().listen();
const player = new Player();

console.log('~~~~~ Welcome to Monsters are Scary ~~~~\n');

console.log('Your starting stats are:');
player.stats();
checkPlayerHealth(player);

