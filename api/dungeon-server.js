'use strict';

const express = require('express');
const Dungeon = require('./models/dungeon');


class DungeonServer {

  constructor() {
    this.dungeon = new Dungeon();
    this.server = express();
    this.server.get('/room/:x/:y', (req, res) => {
      const resource = this.dungeon.getRoomResource({
        x: parseInt(req.params.x),
        y: parseInt(req.params.y) 
      });
      res.set('Content-Type', 'text/plain');
      res.status(200).send(new Buffer(resource));
    });
  }

  listen() {
    console.log('DungeonServer listening on 8080');
    return this.server.listen(8080);
  }

}

module.exports = DungeonServer;

