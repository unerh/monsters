'use strict';

const getRandom = (min,max) => {  
  min = Math.ceil(min);
  max = Math.floor(max);
  return Math.floor(Math.random() * (max - min + 1)) + min;
}

class Dungeon {

  constructor() {
    this.map = new Map();
  }

  static createResource() {
    return (getRandom(1, 100) % 2 === 0) ? 'GOLD' : 'MONSTER';
  }

  // expects object in the form {x:1, y:1}
  getRoomResource(location) {
    const key = JSON.stringify(location);
    let resource = this.map.get(key);
    if (resource === undefined) {
      resource = Dungeon.createResource();
      this.map.set(key, resource);
    }

    return resource;
  }


}

module.exports = Dungeon;

