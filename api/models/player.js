'use strict';

class Player {

  constructor() {
    this.location = {x:0, y:0};
    this.health = 5;
    this.score = 0;
  }

  foundGold() {
    this.score++;
  }

  foundMonster() {
    this.health--;
  }

  isAlive() {
    return this.health > 0;
  }

  move(direction) {
    const movements = {
      'east': (loc) => loc.x++,
      'west': (loc) => loc.x--,
      'north': (loc) => loc.y++,
      'south': (loc) => loc.y--
    };

    movements[direction](this.location);
    console.log('Player is at: ', this.location);
  }

  stats() {
    console.log(`Health: ${this.health}, Score: ${this.score}\n`);
  }
}

module.exports = Player;

