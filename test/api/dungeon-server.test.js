'use strict';

const chai = require('chai');
const expect = chai.expect;
const request = require('supertest-as-promised');

describe('DungeonServer', () => {
  let server;
  let listener;

  beforeEach(() => {
    const DungeonServer = require('../../api/dungeon-server');
    const dungeonServer = new DungeonServer();
    listener = dungeonServer.listen();
    server = dungeonServer.server;
  });
  afterEach(() => {
    listener.close();
  });

  it('responds with text/plain', () => {
    return request(server).get('/room/10/20').set('Accept', 'text/plain').
           expect(200).
           end((err, res) => {
             expect(res.header['Content-Type']).to.equal('text/plain');
             expect(res.body).to.match(/GOLD\|MONSTER/);
           });
  });

});
