'use strict';

const chai = require('chai');
const expect = chai.expect;
const Player = require('../../../api/models/player');

describe('Player', () => {
  let player;

  beforeEach(() => {
    player = new Player();
  });

  it('scores gold', () => {
    player.foundGold();
    player.foundGold();
    player.foundGold();
    expect(player.score).to.equal(3);
  });

  it('encounteres monster', () => {
    player.foundMonster();
    expect(player.health).to.equal(4);
    expect(player.isAlive()).to.equal(true);
  });

  it('dies', () => {
    for (let i = 0; i < 5; i++) {
      player.foundMonster();
    }
    expect(player.health).to.equal(0);
    expect(player.isAlive()).to.equal(false);
  });

  it('moves east', () => {
    player.move('east');
    expect(player.location.x).to.equal(1);
    expect(player.location.y).to.equal(0);
  });

  it('moves west', () => {
    player.move('west');
    expect(player.location.x).to.equal(-1);
    expect(player.location.y).to.equal(0);
  });

  it('moves north', () => {
    player.move('north');
    expect(player.location.x).to.equal(0);
    expect(player.location.y).to.equal(1);
  });

  it('moves south', () => {
    player.move('south');
    expect(player.location.x).to.equal(0);
    expect(player.location.y).to.equal(-1);
  });

});

