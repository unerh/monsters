'use strict';

const chai = require('chai');
const expect = chai.expect;
const Dungeon = require('../../../api/models/dungeon');

describe('Dungeon', () => {
  let dungeon;

  beforeEach(() => {
    dungeon = new Dungeon();
  });

  it('creates resource', () => {
    const resource = Dungeon.createResource();
    expect(resource).to.be.oneOf(['GOLD', 'MONSTER']);
  });

  it('room does not exist so is auto-created', () => {
    const resource = dungeon.getRoomResource({x: 10, y: 20});
    expect(resource).to.be.oneOf(['GOLD', 'MONSTER']);
  });

  it('room already exists so resource is returned', () => {
    const resource = dungeon.getRoomResource({x: 10, y: 20});
    const newResource = dungeon.getRoomResource({x: 10, y: 20});
    expect(dungeon.getRoomResource({x:10, y:20})).to.equal(resource);
  });

});


