~=~=~=~=~= Monsters Are Scary - IAG ABD MacGyver Code Challenge ~=~=~=~=~=

# Description

This is a basic console app where a player navigates the boundles rooms of a
two-dimension dungeon.

# Setup

- Node 6.2.0 is a prerequisite
- Install by running 'npm install && npm test'

# Usage

- Execute with: node app.js
- Navigate your player with north/south/east/west, until health runs out

# Assumptions

- This app has been written with the assumption that it was meant to be a
  single player game.
- Also it is assumed that a resource in a dungeon room is not exhausted.
  So if the player passes from the same room again it will encounter the
  same resource.


